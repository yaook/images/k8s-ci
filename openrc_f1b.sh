export OS_AUTH_URL=https://identity-f1b.cloudandheat.com:5000/v3
export OS_PROJECT_NAME="cah-k8s-ci-environment-f1b-1"
export OS_USER_DOMAIN_NAME="Default"
export OS_PROJECT_DOMAIN_ID="default"
export OS_USERNAME="cah-k8s-ci-environment-f1b-1"
export OS_REGION_NAME="f1b"
export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3
# the OS_PASSWORD is expected to be set by the GitLab CI environment
